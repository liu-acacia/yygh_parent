package com.atguigu.yygh.hosp.service;

import com.atguigu.yygh.model.hosp.Department;
import com.atguigu.yygh.vo.hosp.DepartmentQueryVo;
import com.atguigu.yygh.vo.hosp.DepartmentVo;
import org.springframework.data.domain.Page;


import java.util.List;
import java.util.Map;

public interface DepartmentService {
    //保存科室信息
    void save(Map<String,Object> paramMap);
    //查询科室信息
    Page<Department> findPageDepartment(int page, int limit, DepartmentQueryVo departmentQueryVo);

    //根据医院编号查询所有科室列表
    List<DepartmentVo> findDeptTree(String hoscode);
    //删除科室信息
    void remove(String hoscode, String depcode);

    //根据医院编号 和 科室编号查询科室名称
    String getDepName(String hoscode, String depcode);
}
