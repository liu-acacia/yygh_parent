package com.atguigu.yygh.user.service;

import com.atguigu.yygh.vo.user.LoginVo;

import java.util.Map;

public interface UserInfoService {
    //用户手机号登录接口，返回的是用户登录信息
    Map<String, Object> loginUser(LoginVo loginVo);
}
